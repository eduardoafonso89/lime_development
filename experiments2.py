import warnings
warnings.filterwarnings("ignore")
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as c_map
from IPython.display import Image, display
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.applications.xception import Xception, preprocess_input, decode_predictions
from tensorflow.keras.preprocessing import image

import lime
from lime import lime_image
from lime import submodular_pick

from skimage.segmentation import mark_boundaries

np.random.seed(123)
print(f" Version of tensorflow used: {tf.__version__}")

def load_image_data_from_url(url):
    '''
    Function to load image data from online
    '''
    # The local path to our target image
    image_path = keras.utils.get_file(
    "shark.jpg", url
    )

    display(Image(image_path))
    return image_path

image_path = load_image_data_from_url(url = "https://images.unsplash.com/photo-1560275619-4662e36fa65c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1200&q=80")

#Normalizar a imagem
IMG_SIZE = (299, 299)
def transform_image(image_path, size):
    '''
    Function to transform an image to normalized numpy array
    '''
    img = image.load_img(image_path, target_size=size)
    img = image.img_to_array(img)# Transforming the image to get the shape as [channel, height, width]
    img = np.expand_dims(img, axis=0) # Adding dimension to convert array into a batch of size (1,299,299,3)
    img = img/255.0 # normalizing the image to keep within the range of 0.0 to 1.0
    
    return img

normalized_img = transform_image(image_path, IMG_SIZE)

model = Xception(weights="imagenet")

def get_model_predictions(data):
    model_prediction = model.predict(data)
    print(f"The predicted class is : {decode_predictions(model_prediction, top=1)[0][0][1]}")
    return decode_predictions(model_prediction, top=1)[0][0][1]

#plt.imshow(normalized_img[0])
pred_orig = get_model_predictions(normalized_img)

#Predição
model_prediction = model.predict(normalized_img)
top5_pred = decode_predictions(model_prediction, top=5)[0]
for pred in top5_pred:
    print(pred[1])

from sklearn.linear_model import Ridge
from sklearn.utils import check_random_state

model_regres =  Ridge(alpha=1, fit_intercept=True,
                                    random_state=check_random_state(None))

#model_regres = model_regres.fit(normalized_img[:, 1],
#                       None, sample_weight=1000)

f, axarr = plt.subplots(2, 1)

explainer = lime_image.LimeImageExplainer()
exp = explainer.explain_instance(normalized_img[0], 
                                 model.predict, 
                                 top_labels=5, 
                                 hide_color=0, 
                                 num_samples=1000,
                                 num_features=1)

#plt.imshow(exp.segments)
#plt.axis('off')
#plt.show()

image, mask = exp.get_image_and_mask(exp.top_labels[0], 
                                     positive_only=False, 
                                     num_features=6, 
                                     hide_rest=False,
                                     min_weight=0.1
                                     )

axarr[0].imshow(mark_boundaries(image, mask))

explainer2 = lime_image.LimeImageExplainer()
exp2 = explainer2.explain_instance_modified(normalized_img[0], 
                                 model.predict, 
                                 top_labels=5, 
                                 hide_color=0, 
                                 num_samples=1000,
                                 num_features=1)

#plt.imshow(exp.segments)
#plt.axis('off')
#plt.show()

image2, mask2 = exp.get_image_and_mask(exp2.top_labels[0], 
                                     positive_only=False, 
                                     num_features=6, 
                                     hide_rest=False,
                                     min_weight=0.1
                                     )

axarr[1].imshow(mark_boundaries(image2, mask2))

plt.show()