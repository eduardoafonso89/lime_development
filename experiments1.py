import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.datasets import mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train = x_train.reshape((-1,28,28,1)).astype('float32') / 255.0
x_test = x_test.reshape((-1,28,28,1)).astype('float32') / 255.0

import numpy as np
def to_rgb(x):
    x_rgb = np.zeros((x.shape[0], 28, 28, 3))
    for i in range(3):
        x_rgb[..., i] = x[..., 0]
    return x_rgb
x_train = to_rgb(x_train)
x_test = to_rgb(x_test)


model = keras.Sequential(
    [
     keras.Input(shape=(28,28,3)),
     layers.Conv2D(16, 3, activation='relu'),
     layers.MaxPooling2D(),
     layers.Flatten(),
     layers.Dense(10)
    ]
)

model.compile(
  loss=keras.losses.SparseCategoricalCrossentropy(from_logits=True),
  optimizer=keras.optimizers.Adam(),
  metrics=['accuracy']
)

model.fit(
        x_train, 
        y_train, 
        epochs=2, 
        batch_size=32, 
        validation_data = (x_test, y_test))

from lime import lime_image
from skimage.segmentation import mark_boundaries
import matplotlib.pyplot as plt

for i in range(10):
  print(model.predict(
                x_train[i].reshape((1,28,28,3))
          ).argmax(axis=1)[0])
  #plt.imshow(x_train[i])
  #plt.show()

explainer = lime_image.LimeImageExplainer(feature_selection='lasso_path', verbose=False, random_state=42)

f, axarr = plt.subplots(2, 10)

for i in range(10):
  explanation = explainer.explain_instance(
          x_train[i], 
          model.predict,
          num_samples=100
  )
  plt.imshow(x_train[i])
  image, mask = explanation.get_image_and_mask(
          model.predict(
                x_train[i].reshape((1,28,28,3))
          ).argmax(axis=1)[0],
          positive_only=True, 
          hide_rest=False,
          min_weight=0.1)

  #plt.imshow(mark_boundaries(image, mask))
  axarr[0][i].imshow(mark_boundaries(image, mask))

  explanation = explainer.explain_instance_modified(
          x_train[i], 
          model.predict,
          num_samples=100
  )
  plt.imshow(x_train[i])
  image, mask = explanation.get_image_and_mask(
          model.predict(
                x_train[i].reshape((1,28,28,3))
          ).argmax(axis=1)[0],
          positive_only=True, 
          hide_rest=False,
          min_weight=0.1)
  axarr[1][i].imshow(mark_boundaries(image, mask))

  
plt.show()
